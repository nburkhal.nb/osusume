# Osusume

Personalized Anime Recommender that is unlike any others.

## ETL

![ETL](https://i.imgur.com/7WvzMiA.png)

## TO-DO

### Phase 0. Pre-ETL
    - Using sample data do:
      - [] Data Wrangling <-> Cleaning
      - [] Train/Test & Evaluate Model
      - [] Compare different recommenders

### Phase 1. Start ETL 

#### Part 1. (Data Mining into Data Lake)

### Phase 2. Train/Test & Evaluate Model

### Phase 3. Deploy APIs

### Phase 4. Combined Front End <-> Back End


